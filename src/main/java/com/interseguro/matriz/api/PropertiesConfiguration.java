package com.interseguro.matriz.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
	@PropertySource("classpath:local.mensajes.properties")
})
public class PropertiesConfiguration {

}
