package com.interseguro.matriz.api.errors;

public class MatrizNoAdecuadaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MatrizNoAdecuadaException(String matriz) {
		super("La matriz bidimensional no cumple con el requisito establecido "+matriz);
	}
	
}
