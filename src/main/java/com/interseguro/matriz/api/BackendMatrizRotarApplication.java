package com.interseguro.matriz.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendMatrizRotarApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(BackendMatrizRotarApplication.class, args);
	}

}
