package com.interseguro.matriz.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.interseguro.matriz.api.errors.MatrizNoAdecuadaException;
import com.interseguro.matriz.api.mappers.interfaces.AccionablesInterface;

@Controller
@RequestMapping(path = "/app")
public class RotarMatrizController {

	@Autowired
	@Qualifier(value = "matrizNxN")
	private AccionablesInterface<Integer> accionable;

	@ResponseBody
	@PostMapping(path = {"/",""}, consumes = "application/json")
	public List<List<Integer>> index(@RequestBody(required = true) List<List<Integer>> lista) {
		if (!accionable.validarMatriz(lista)) {
			throw new MatrizNoAdecuadaException(lista.toString());
		}
		return accionable.rotarMatriz(lista);
	}

}
