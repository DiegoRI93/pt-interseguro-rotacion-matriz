package com.interseguro.matriz.api.controller.errorHandler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.interseguro.matriz.api.errors.MatrizNoAdecuadaException;

@ControllerAdvice()
public class ErrorHandlerController {

	@Value("${mesaje.datos.numericos.incorrectos}")
	private String errorNumerico;

	@Value("${mensaje.error.fila.columna.longitud.diferente}")
	private String errorMatriz;

	@Value("${mensaje.error.general}")
	private String errorGeneral;

	@ResponseBody
	@ExceptionHandler(InvalidFormatException.class)
	public Map<String, String> datosInvalidado(Exception ex) {
		Map<String, String> map = new HashMap<>();
		map.put("mesaje", errorNumerico);
		map.put("mensajeError", ex.getMessage());
		map.put("Status", HttpStatus.BAD_REQUEST.toString());
		return map;
	}

	@ResponseBody
	@ExceptionHandler(MatrizNoAdecuadaException.class)
	public Map<String, String> matrizNoAdecuada(MatrizNoAdecuadaException matriz) {
		Map<String, String> map = new HashMap<>();
		map.put("mesaje", errorMatriz);
		map.put("mensajeError", matriz.getMessage());
		map.put("Status", HttpStatus.BAD_REQUEST.toString());
		return map;
	}

	@ResponseBody
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public Map<String, String> matrizNoAdecuada(Exception ex) {
		Map<String, String> map = new HashMap<>();
		map.put("mesaje", errorGeneral);
		map.put("mensajeError", ex.getMessage());
		map.put("Status", HttpStatus.BAD_REQUEST.toString());
		return map;
	}
}
