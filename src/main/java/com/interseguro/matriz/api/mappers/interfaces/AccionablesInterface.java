package com.interseguro.matriz.api.mappers.interfaces;

import java.util.List;

public interface AccionablesInterface <T> {

	public List<List<T>> rotarMatriz(List<List<T>> lista);

	public Boolean validarMatriz(List<List<T>> lista);

}
