package com.interseguro.matriz.api.mappers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSOutput;

import com.interseguro.matriz.api.mappers.interfaces.AccionablesInterface;

@Component(value = "matrizNxN")
public class AccionablesImplementacion implements AccionablesInterface<Integer> {

	@Override
	public List<List<Integer>> rotarMatriz(List<List<Integer>> lista) {

		int longitud = lista.size();

		Integer[][] aux = new Integer[longitud][longitud];
		Integer[][] ls = new Integer[longitud][longitud];

		for (int x = 0; x < longitud; x++) {
			for (int y = 0; y < longitud; y++) {
				ls[x][y] = lista.get(x).get(y);
			}
		}

		for (int x = 0; x < longitud; x++) {
			for (int y = 0; y < longitud; y++) {
				aux[longitud - 1 - x][y] = ls[y][x];
			}
			lista.set(x, Arrays.asList(aux[x]));
		}
		return lista;
	}

	@Override
	public Boolean validarMatriz(List<List<Integer>> lista) {
		Stream<List<Integer>> flag = lista.stream().filter(prin -> (prin.size() == lista.size()));
		return flag.count()==lista.size();
	}

}
