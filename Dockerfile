FROM openjdk:8-jdk-alpine
LABEL mantainer "jdroldan93@gmail.com"
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} backend-matriz-rotar-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java","-jar","backend-matriz-rotar-0.0.1-SNAPSHOT.jar" ]